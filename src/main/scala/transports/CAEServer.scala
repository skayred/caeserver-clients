package transports

import org.apache.cxf.jaxws.endpoint.dynamic.JaxWsDynamicClientFactory
import javax.xml.namespace.QName
import javax.xml.bind.JAXBContext
import java.io.StringWriter
import lib.XMLFormatter

/**
 * Created with IntelliJ IDEA.
 * User: dmitrysavchenko
 * Date: 5/5/13
 * Time: 9:28 PM
 * To change this template use File | Settings | File Templates.
 */
class CAEServer(endpointURL: String) extends Transport {
  val dcf = JaxWsDynamicClientFactory.newInstance();
  private val client = dcf.createClient(sanitizeURL(endpointURL) + "caeserver?wsdl");

  def index(projectID: String) { println(client) }

  def indexAll() {
    client.invoke(new QName("http://java.CAEServer", "indexAllProjects"));
  }

  def createInstance(projectID: String) {}

  def GetProblemCAEBeansList(projectID: String) {
    val res = client.invoke(new QName("http://java.CAEServer", "getProblemCAEBeans"), projectID);

    println(res{0})
  }

  def getProblemCAEBean(projectID: String, problemID: String) {
    val res = client.invoke(new QName("http://java.CAEServer", "getProblemCAEBeanByID"), projectID, problemID);

    val getProblemCaebeanMethod = res{0}.getClass().getMethod("getProblemCaebean")

    val problemCAEBean = getProblemCaebeanMethod.invoke(res{0})

    val context = JAXBContext.newInstance(problemCAEBean.getClass())
    val marhshaller = context.createMarshaller()

    val xmlStringWriter = new StringWriter()

    marhshaller.marshal(problemCAEBean, xmlStringWriter)

    println(new XMLFormatter().format(xmlStringWriter.toString()))
  }
}
