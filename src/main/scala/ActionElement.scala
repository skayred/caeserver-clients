/**
 * Created with IntelliJ IDEA.
 * User: dmitrysavchenko
 * Date: 5/7/13
 * Time: 9:25 PM
 * To change this template use File | Settings | File Templates.
 */
case class ActionElement (
  description: String
  , usage: String
  , action: Array[String] => Unit
)
