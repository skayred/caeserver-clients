import transports.{CAEInstance, CAEServer}

object MainConsole {
  /**
   *  Available actions. Key - name, value - map of description and action lambda
   * */
   val actions = Map(
    "index" ->  ActionElement(
      "Makes CAEServer index project with provided project_id "
      , "index project_id"
      , (args: Array[String]) => {
          if (checkArgument(3, args, "Project ID wasn't specified. Please supply project ID.")) {
            new CAEServer(args{0}).index(args{2})
          }
        }
    )
    , "index-all" -> ActionElement(
      "Makes CAEServer index all projects in it's project directory"
      , "index-all"
      , (args: Array[String]) => {
          new CAEServer(args{0}).indexAll()
        }
    )
    , "run" -> ActionElement(
      "Runs virtual experiment with provided problem_caebean.xml as problem CAEBean"
      , "run problem_caebean.xml"
      , (args: Array[String]) => {
          if (checkArgument(3, args, "Problem CAEBean XML file wasn't specified. Please provide valid path.")) {
            new CAEInstance(args{0}).run(args{2})
          }
        }
    )
    , "status" -> ActionElement(
      "Returns status of the experiment with provided instance_id"
      , "status instance_id"
      , (args: Array[String]) => {
          if (checkArgument(3, args, "Instance ID wasn't specified. Please supply instance ID.")) {
            new CAEInstance(args{0}).getStatus(args{2})
          }
        }
    )
    , "create-instance" -> ActionElement(
      "Creates instance for the project with provided project_id"
      , "create-instance project_id"
      , (args: Array[String]) => {
          if (checkArgument(3, args, "Project ID wasn't specified. Please supply project ID.")) {
            new CAEServer(args{0}).createInstance(args{2})
          }
        }
    )
    , "list-problems" -> ActionElement(
      "Gets list of available problem CAEBeans"
      , "list-problems project_id"
      , (args: Array[String]) => {
        if (checkArgument(3, args, "Project ID wasn't specified. Please supply project ID.")) {
          new CAEServer(args{0}).GetProblemCAEBeansList(args{2})
        }
      }
    )
    , "get-problem" -> ActionElement(
      "Get problem CAEBean of the project project_id"
      , "get-problem project_id"
      , (args: Array[String]) => {
        if (checkArgument(3, args, "Project ID wasn't specified. Please supply project ID.")
            && checkArgument(4, args, "Problem ID wasn't specified. Please supply project ID.")) {
          new CAEServer(args{0}).getProblemCAEBean(args{2}, args{3});
        }
      }
    )
  )

  private def checkArgument(number: Int, args: Array[String], message: String): Boolean = {
    if (args.length < number) {
      System.err.println(message)

      false
    } else { true }
  }

  def printReference() {
    var reference = "Wrong argument number. Please specify valid host and action\n\nUsage: caeserver-client action\n\nAvailable actions:\n\n"

    actions.foreach { case(key, value) => {
      reference += "\t%s -- %s\n".format(value.usage, value.description)
    }}

    System.err.println(reference)
  }

  def main(args: Array[String]) {
    if (args.length < 2) {
      printReference()
      return
    }

    val providedAction = args{1}

    if (actions.contains(providedAction)) {
      actions{providedAction}.action(args)
    } else {
      printReference()
    }
  }
}